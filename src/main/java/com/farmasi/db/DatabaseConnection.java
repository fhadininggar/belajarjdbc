/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class DatabaseConnection {

    private static DatabaseConnection instance;
    private Connection connection;
    private String url = "jdbc:mysql://localhost:3306/farmasi";
    private String nama = "root";
    private String pass = "root";

    private DatabaseConnection() throws ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(url, nama, pass);
        } catch (SQLException ex) {
            System.out.println("gagal koneksi k database coy" + ex.getLocalizedMessage());
        }
        
    }

    public Connection getConnection() {
        return connection;

    }
    
    public static DatabaseConnection getInstance() throws ClassNotFoundException, SQLException  {
        if (instance == null) {
            instance = new DatabaseConnection();
        }else if (instance.getConnection().isClosed()){
            instance = new DatabaseConnection();
        }
    return instance;
    }
            
    
}

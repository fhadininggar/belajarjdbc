
CREATE TABLE category (
                category_id INTEGER AUTO_INCREMENT NOT NULL,
                name VARCHAR(55) NOT NULL,
                PRIMARY KEY (category_id)
);


CREATE TABLE product (
                product_id INTEGER AUTO_INCREMENT NOT NULL,
                category_id INTEGER NOT NULL,
                name VARCHAR(155) NOT NULL,
                description VARCHAR(255) NOT NULL,
                price DOUBLE PRECISION NOT NULL,
                
                expiredDate DATE NOT NULL,
                PRIMARY KEY (product_id, category_id)
);


ALTER TABLE product ADD CONSTRAINT category_product_fk
FOREIGN KEY (category_id)
REFERENCES category (category_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
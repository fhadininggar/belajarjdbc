/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.model;

import java.util.Date;



/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class Product {
    private int product_id;
    private String name;
    private Category category;
    private String description;
    private double price;
    private Date expiredDate;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
    
    
}

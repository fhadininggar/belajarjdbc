/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.model;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class Category {
    private int category_id;
    private String name;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}

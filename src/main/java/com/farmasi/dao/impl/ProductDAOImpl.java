/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.dao.impl;

import com.farmasi.dao.ProductDAO;
import com.farmasi.model.Category;
import com.farmasi.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class ProductDAOImpl implements ProductDAO {

    private Connection connection;
    
    public ProductDAOImpl(Connection connection) {
        this.connection = connection;
        
        
    }
    
    @Override
    public int save(Product product) throws SQLException {
        
        String sql = "insert into product(category_id, name, description, price, expiredDate ) values(?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        if (product.getCategory() != null) {
            statement.setInt(1, product.getCategory().getCategory_id());
        } else {
            throw new SQLException("data category blm ada");
        }
        statement.setString(2, product.getName());
        statement.setString(3, product.getDescription());
        statement.setDouble(4, product.getPrice());
        statement.setDate(5, new java.sql.Date(product.getExpiredDate().getTime()));
        return statement.executeUpdate();
    }
    
    @Override
    public int update(int i, Product product) throws SQLException {
       String sql = "UPDATE product set name=?, description=?, price=?, expiredDate=? where product_id=?";
       PreparedStatement statement = connection.prepareStatement(sql);
       statement.setString(1, product.getName());
       statement.setString(2, product.getDescription());
       statement.setDouble(3, product.getPrice());
       statement.setDate(4, new java.sql.Date(product.getExpiredDate().getTime()));
       statement.setInt(5, i);
       
       
       return statement.executeUpdate();
    }
    
    @Override
    public int delete(int i) throws SQLException {
       String sql ="delete from product where product_id=?";
       PreparedStatement statement=connection.prepareStatement(sql);
       statement.setInt(1, i);
       
       return statement.executeUpdate();
    }
    
    @Override
    public Product findByProduct_id(int i) throws SQLException {
        
        String sql = " SELECT p.product_id,p.name,p.description,p.price,p.expireddate,p.category_id,c.name as category_name "
                + " FROM product p INNER JOIN category c ON (p.category_id = c.category_id) "
                + " Where p.product_id = ? ";
        
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, i);
        ResultSet resultSet = statement.executeQuery();
        //List<Product> list = new ArrayList<>();
        
        Product prod = null;
        if (resultSet.next()) {
            
            Category cat = new Category();
            cat.setCategory_id(resultSet.getInt("category_id"));
            cat.setName(resultSet.getString("category_name"));
            
            prod = new Product();
            prod.setCategory(cat);
            prod.setDescription(resultSet.getString("description"));
            prod.setExpiredDate(resultSet.getDate("expireddate"));
            prod.setProduct_id(resultSet.getInt("product_id"));
            prod.setName(resultSet.getString("name"));
            prod.setPrice(resultSet.getDouble("price"));

            //list.add(prod);
        }
        return prod;
    }
    
    @Override
    public List<Product> findAll() throws SQLException {
        String sql = "SELECT  p.product_id, p.name, p.description, p.price, p.expiredDate, c.category_id, c.name as category_name " 
                + " FROM product p INNER JOIN category c ON p.category_id= c.category_id order by p.product_id asc";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        
        List<Product> list = new ArrayList<Product>();
        while (result.next()) {
            Category c= new Category();
            c.setCategory_id(result.getInt("category_id"));
            c.setName(result.getString("category_name"));
            
            Product p = new Product();
            
          
            p.setProduct_id(result.getInt("product_id"));
            p.setName(result.getString("name"));
            p.setDescription(result.getString("description"));
            p.setPrice(result.getDouble("price"));
            p.setExpiredDate(result.getDate("expiredDate"));
           
            p.setCategory(c);
            list.add(p);
        }        
        return list;        
    }
    
    @Override
    public List<Product> findByName(String name) throws SQLException {
        String sql = "SELECT * FROM product where name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        Product c;
        List<Product> list = new ArrayList<Product>();
        while (result.next()) {
            c = new Product();
            c.setProduct_id(result.getInt("product_id"));
            c.setName(result.getString("name"));
            c.setDescription(result.getString("description"));
            c.setPrice(result.getDouble("price"));
            c.setExpiredDate(result.getDate("expiredDate"));
            list.add(c);
        }
        return list;
    }
}

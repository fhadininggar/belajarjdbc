/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.dao.impl;

import com.farmasi.dao.CategoryDAO;
import com.farmasi.model.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class CategoryDAOImpl implements CategoryDAO {
    private Connection connection;
    
    public CategoryDAOImpl (Connection connection) {
        this.connection = connection;
    }

    @Override
    public int save(Category category) throws SQLException {
        String sql = "INSERT INTO category (name) VALUES (?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,category.getName());
        return statement.executeUpdate();
        
    }

    @Override
    public Category findByCategory_id(int i) throws SQLException {
        String sql = "SELECT * FROM category where category_id=?";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        Category c = new Category();
        if (result.next()){
            c.setCategory_id(result.getInt("category_id"));
            c.setName(result.getString("name"));
                    }
        return c;
    }

    @Override
    public List<Category> findAll() throws SQLException {
        String sql ="SELECT * FROM category";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        Category c;
        List <Category> list = new ArrayList<Category>();
        while (result.next()) {
         c = new Category();
            c.setCategory_id(result.getInt("category_id")  );
            c.setName(result.getString("name")  );
            list.add (c);
        }                
        return list ;   
        }
    

    @Override
    public List<Category> findByName(String name) throws SQLException {
        String sql = "SELECT * FROM category where name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        Category c;
        List <Category> list = new ArrayList<Category>();
        while (result.next()) {
            c = new Category();
            c.setCategory_id(result.getInt("category_id"));
            c.setName(result.getString("name"));
            list.add (c);
        }
        return list;
    }

    @Override
    public int update(int i, Category category) throws SQLException {
        String sql ="update Category set nama =? where category_id=? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, category.getName());
        statement.setInt(2, i);
        
        return  statement.executeUpdate();
    }

    @Override
    public int delete(int i) throws SQLException {
        String sql ="delete from Category where category_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1  , i);
        return statement.executeUpdate();    
    }
    
    
}

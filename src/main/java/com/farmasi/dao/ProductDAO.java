/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.dao;

import com.farmasi.model.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public interface ProductDAO {
     int save(Product product) throws SQLException;

    int update(int i, Product product) throws SQLException;

    int delete(int i) throws SQLException;

    Product  findByProduct_id(int i) throws SQLException;

    List<Product > findAll() throws SQLException;

    List<Product > findByName(String name) throws SQLException;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.dao;

import com.farmasi.model.Category;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public interface CategoryDAO {
    
    int save (Category category) throws SQLException;
    
    int update (int i, Category category) throws SQLException;
    
    int delete ( int i) throws SQLException;
    
    Category findByCategory_id(int i) throws SQLException;
    
    List<Category> findAll() throws SQLException;
    
    List<Category> findByName (String name) throws SQLException;
    
    
    
    
}

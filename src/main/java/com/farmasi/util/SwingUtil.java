/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.util;

import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public final class SwingUtil {
    
    public static JFrame getParentJFrame (JComponent component) {
        Container parent = component;
        do {
            parent = (Container) parent.getParent();
        } while (!(parent instanceof JFrame));
        return ((JFrame) parent);
    }
    
}

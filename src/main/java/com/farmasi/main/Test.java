/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farmasi.main;

import com.farmasi.dao.CategoryDAO;
import com.farmasi.dao.impl.CategoryDAOImpl;
import com.farmasi.db.DatabaseConnection;
import com.farmasi.model.Category;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rm C.Kristianto_SCJ
 */
public class Test {

    public static void main(String[] args) {
        /* Hello */
        try {
            CategoryDAO categoryDAO = new CategoryDAOImpl(DatabaseConnection.getInstance().getConnection());

            Category c = new Category();
            c.setName("basengggg");
            categoryDAO.save(c);

List<Category> allData = categoryDAO.findAll();
for(int i=0; i < allData.size(); i++) {
    c = allData.get(i);
    System.out.println("id= " + c.getCategory_id()+ "name= " + c.getName());
}
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }



    }
}
